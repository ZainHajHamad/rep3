package testEmployeeApi;

import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.ArrayList;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import java.util.List;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
public class DeleteRequestTesting {
	@Test
	public void deleteEmployeeByValidID_Tdelete1() throws Exception{
         //The server is empty, so we have to POST Employee data and after that delete it.
		 RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification post_request = RestAssured.given();
		 
		 JSONObject requestParams = new JSONObject();
		 //Casting
		 requestParams.put("firstName", "Zain"); 
		 requestParams.put("lastName", "Haj Hamad");
		 requestParams.put("id", "438768422148");
		 requestParams.put("salary", 6000);
		 
		 post_request.body(requestParams.toJSONString());
		 Response post_response = post_request.post("/zain-t3");
		 
		 //After posting new employee, we have to check DELETE method.
         String id = "438768422148";
		 
		 RestAssured.baseURI = "http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification delete_request = RestAssured.given(); 
		 
		 //Add a header stating the Request body is a JSON
		 delete_request.header("Content-Type", "application/json"); 
		 
		  //Delete the request and check the response
		 Response delete_response = delete_request.delete("/zain-t3/"+ id); 
		  int statusCode=delete_response.getStatusCode();
		  System.out.println("Status code is: "+statusCode);
		  Assert.assertEquals(statusCode, 200);
		  
		  String responseBody=delete_response.getBody().asString();
		  System.out.println("Response Body for deleting by valid ID:" +responseBody);
		 
		  //status line verification
		  String statusLine=delete_response.getStatusLine();
		  System.out.println("Status line is:"+statusLine);
		  Assert.assertEquals(statusLine, "HTTP/1.1 200 CatoOK");
		  
		  //After that, I use the GET method to check whether the body is empty or not
		  //Specify base URI
		  RestAssured.baseURI="http://192.168.200.91:8080/demo-server/employee-module";
		  
		  //Request object
		  RequestSpecification httpRequest=RestAssured.given();
		  
		  //Response object
		  Response get_response=httpRequest.request(Method.GET,"/zain-t3");
		  
		  //print response in console window
		  
		  String responseBodyForGet=get_response.getBody().asString();
		  System.out.println("Response Body of getting all employees after deletion by valid ID:" +responseBodyForGet);
	
		  BufferedReader br=null;
          ObjectMapper objectMapper=new ObjectMapper();
		    try {
		    	
		    	  //take the actual data from the server and convert it from JSON format to java object called allInfo
		          AllInfo allInfo= objectMapper.readValue(responseBodyForGet,AllInfo.class);
		          System.out.println("maxSalary= "+ allInfo.getMaxSalary() + "and totalSalaries =" + allInfo.getTotalSalaries() + "and employeesCount =" + allInfo.getEmployeesCount() + "and minSalary =" +allInfo.getMinSalary());
		          
		        
		          if(allInfo != null) {
		          for(Employee e : allInfo.getItems() ) {
		        	  System.out.println(e.getID()+" - "+ e.getFirstName()+" - "+ e.getLastName()+ " - "+ e.getSalary());
		          }
		          }

		          //take the expected data from the file and convert it from JSON format to java object called allInfo_fromFile
		    	  br= new BufferedReader(new FileReader("C:\\Users\\zainh\\eclipse-workspace1\\testEmployeeApi\\ExpectedBodyAfterDeleteByValidID.txt"));
		    	  AllInfo allInfo_fromFile= objectMapper.readValue(br.toString(),AllInfo.class);
		    	  System.out.println("maxSalary= "+ allInfo_fromFile.getMaxSalary() + "and totalSalaries =" + allInfo_fromFile.getTotalSalaries() + "and employeesCount =" + allInfo_fromFile.getEmployeesCount() + "and minSalary =" +allInfo_fromFile.getMinSalary());
		           
		            if(allInfo_fromFile != null) {
		            for(Employee e1 : allInfo_fromFile.getItems() ) {
		          	  System.out.println(e1.getID()+" - "+ e1.getFirstName()+" - "+ e1.getLastName()+ " - "+ e1.getSalary());
		            }
		            }   
	                
		            //comparing between two objects ( which are allInfo and allInfo_fromFile).
			         IsEqualFunctions r1 = new IsEqualFunctions() ;
			         Assert.assertEquals(true,r1.is_equal_objects(allInfo, allInfo_fromFile));
	          }//end of try
		    
	          catch(FileNotFoundException e) {
	      		e.printStackTrace();
	      	}
		  
}

	@Test
	public void deleteEmployeeByInvalidID_Tdelete2() throws Exception{
         //The server is empty, so we have to POST Employee data and after that delete it.
		 RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification post_request = RestAssured.given();
		 
		 JSONObject requestParams = new JSONObject();
		 //Casting
		 requestParams.put("firstName", "Zain"); 
		 requestParams.put("lastName", "Haj Hamad");
		 requestParams.put("id", "438768422148");
		 requestParams.put("salary", 6000);
		 
		 post_request.body(requestParams.toJSONString());
		 Response post_response = post_request.post("/zain-t3");
		 
		 //After posting new employee, we have to check DELETE method.
         String id = "438768422111"; //choose invalid ID
		 
		 RestAssured.baseURI = "http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification delete_request = RestAssured.given(); 
		 
		 //Add a header stating the Request body is a JSON
		 delete_request.header("Content-Type", "application/json"); 
		 
		  //Delete the request and check the response
		  Response delete_response = delete_request.delete("/zain-t3/"+ id); 
		  int statusCode=delete_response.getStatusCode();
		  System.out.println("Status code is: "+statusCode);
		  Assert.assertEquals(statusCode, 200);
		  
		  String responseBody=delete_response.getBody().asString();
		  System.out.println("Response Body for deleting by invalid ID:" +responseBody);
		 
		  //status line verification
		  String statusLine=delete_response.getStatusLine();
		  System.out.println("Status line is:"+statusLine);
		  Assert.assertEquals(statusLine, "HTTP/1.1 200 CatoOK");
		  
		  //Use the GET method to check whether the body is empty or not
		  //Specify base URI
		  RestAssured.baseURI="http://192.168.200.91:8080/demo-server/employee-module";
		  
		  //Request object
		  RequestSpecification httpRequest=RestAssured.given();
		  
		  //Response object
		  Response get_response=httpRequest.request(Method.GET,"/zain-t3");
		  
		  //print response in console window
		  
		  String responseBodyForGet=get_response.getBody().asString();
		  System.out.println("Response Body of getting all employees after deletion by Invalid ID:" +responseBodyForGet);
		  
		  BufferedReader br=null;
          ObjectMapper objectMapper=new ObjectMapper();
		    try {
		    	
		    	  //take the actual data from the server and convert it from JSON format to java object called allInfo
		          AllInfo allInfo= objectMapper.readValue(responseBodyForGet,AllInfo.class);
		          System.out.println("maxSalary= "+ allInfo.getMaxSalary() + "and totalSalaries =" + allInfo.getTotalSalaries() + "and employeesCount =" + allInfo.getEmployeesCount() + "and minSalary =" +allInfo.getMinSalary());
		          
		        
		          if(allInfo != null) {
		          for(Employee e : allInfo.getItems() ) {
		        	  System.out.println(e.getID()+" - "+ e.getFirstName()+" - "+ e.getLastName()+ " - "+ e.getSalary());
		          }
		          }

		          //take the expected data from the file and convert it from JSON format to java object called allInfo_fromFile
		    	  br= new BufferedReader(new FileReader("C:\\Users\\zainh\\eclipse-workspace1\\testEmployeeApi\\ExpectedBodyAfterUpdatingByInvalidID.txt"));
		    	  AllInfo allInfo_fromFile= objectMapper.readValue(br.toString(),AllInfo.class);
		    	  System.out.println("maxSalary= "+ allInfo_fromFile.getMaxSalary() + "and totalSalaries =" + allInfo_fromFile.getTotalSalaries() + "and employeesCount =" + allInfo_fromFile.getEmployeesCount() + "and minSalary =" +allInfo_fromFile.getMinSalary());
		           
		            if(allInfo_fromFile != null) {
		            for(Employee e1 : allInfo_fromFile.getItems() ) {
		          	  System.out.println(e1.getID()+" - "+ e1.getFirstName()+" - "+ e1.getLastName()+ " - "+ e1.getSalary());
		            }
		            }   
	                
		            //comparing between two objects ( which are allInfo and allInfo_fromFile).
			         IsEqualFunctions r1 = new IsEqualFunctions() ;
			         Assert.assertEquals(true,r1.is_equal_objects(allInfo, allInfo_fromFile));
	          }//end of try
		    
	          catch(FileNotFoundException e) {
	      		e.printStackTrace();
	      	}
		  
		  
	
}
	
	@Test
	public void deleteAllEmployees_Tdelete3() throws Exception{
		//The server is empty, so I have to POST many employees to the list and after that delete them all.
		//posting the first employee
		 RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification first_post_request = RestAssured.given();
		 
		 JSONObject request1Params = new JSONObject();
		 //Casting
		 request1Params.put("firstName", "Zain"); 
		 request1Params.put("lastName", "Haj Hamad");
		 request1Params.put("id", "438768422148");
		 request1Params.put("salary", 6000);
		 
		 first_post_request.body(request1Params.toJSONString());
		 Response first_post_response = first_post_request.post("/zain-t3");
		
		 //posting the second employee
		 RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification second_post_request = RestAssured.given();
		 
		 JSONObject request2Params = new JSONObject();
		 //Casting
		 request2Params.put("firstName", "John"); 
		 request2Params.put("lastName", "Smith");
		 request2Params.put("id", "438768422146");
		 request2Params.put("salary", 5000);
		 
		 second_post_request.body(request2Params.toJSONString());
		 Response second_post_response = second_post_request.post("/zain-t3");
		 
		 //deleting first and second employees
		 RestAssured.baseURI = "http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification delete_request = RestAssured.given(); 
		 
		 //Add a header stating the Request body is a JSON
		 delete_request.header("Content-Type", "application/json"); 
		 
		  //Delete the request and check the response
		 Response delete_response = delete_request.delete("/zain-t3"); 
		  int statusCode=delete_response.getStatusCode();
		  System.out.println("Status code is: "+statusCode);
		  Assert.assertEquals(statusCode, 200);
		  
		  String responseBody=delete_response.getBody().asString();
		  System.out.println("Response Body for deleting all employees:" +responseBody);
		 
		  //status line verification
		  String statusLine=delete_response.getStatusLine();
		  System.out.println("Status line is:"+statusLine);
		  Assert.assertEquals(statusLine, "HTTP/1.1 200 CatoOK");
		  
		  //Use the GET method to check whether the body is empty or not
		  //Specify base URI
		  RestAssured.baseURI="http://192.168.200.91:8080/demo-server/employee-module";
		  
		  //Request object
		  RequestSpecification httpRequest=RestAssured.given();
		  
		  //Response object
		  Response get_response=httpRequest.request(Method.GET,"/zain-t3");
		  
		  //print response in console window
		  
		  String responseBodyForGet=get_response.getBody().asString();
		  System.out.println("Response Body of getting all employees after deletion all Employees:" +responseBodyForGet);
		  
		  
		  BufferedReader br=null;
          ObjectMapper objectMapper=new ObjectMapper();
		    try {
		    	
		    	  //take the actual data from the server and convert it from JSON format to java object called allInfo
		          AllInfo allInfo= objectMapper.readValue(responseBodyForGet,AllInfo.class);
		          System.out.println("maxSalary= "+ allInfo.getMaxSalary() + "and totalSalaries =" + allInfo.getTotalSalaries() + "and employeesCount =" + allInfo.getEmployeesCount() + "and minSalary =" +allInfo.getMinSalary());
		          
		        
		          if(allInfo != null) {
		          for(Employee e : allInfo.getItems() ) {
		        	  System.out.println(e.getID()+" - "+ e.getFirstName()+" - "+ e.getLastName()+ " - "+ e.getSalary());
		          }
		          }

		          //take the expected data from the file and convert it from JSON format to java object called allInfo_fromFile
		    	  br= new BufferedReader(new FileReader("C:\\Users\\zainh\\eclipse-workspace1\\testEmployeeApi\\ExpectedBodyAfterDeleteAllEmployees.txt"));
		    	  AllInfo allInfo_fromFile= objectMapper.readValue(br.toString(),AllInfo.class);
		    	  System.out.println("maxSalary= "+ allInfo_fromFile.getMaxSalary() + "and totalSalaries =" + allInfo_fromFile.getTotalSalaries() + "and employeesCount =" + allInfo_fromFile.getEmployeesCount() + "and minSalary =" +allInfo_fromFile.getMinSalary());
		           
		            if(allInfo_fromFile != null) {
		            for(Employee e1 : allInfo_fromFile.getItems() ) {
		          	  System.out.println(e1.getID()+" - "+ e1.getFirstName()+" - "+ e1.getLastName()+ " - "+ e1.getSalary());
		            }
		            }   
	               
		            //comparing between two objects ( which are allInfo and allInfo_fromFile).
			         IsEqualFunctions r1 = new IsEqualFunctions() ;
			         Assert.assertEquals(true,r1.is_equal_objects(allInfo, allInfo_fromFile));
	          }//end of try
		    
	          catch(FileNotFoundException e) {
	      		e.printStackTrace();
	      	}
	}
	}

