package testEmployeeApi;

import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.ArrayList;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import java.util.List;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
public class PutRequestTesting {

	@Test
	public void updateEmployeeInfoByValidID_Tput1() throws Exception{
	
		//The server is empty, so we have to POST Employee data and after that update it.
		 RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification post_request = RestAssured.given();
		 
		 JSONObject requestParams = new JSONObject();
		 //Casting
		 requestParams.put("firstName", "Zain"); 
		 requestParams.put("lastName", "Haj Hamad");
		 requestParams.put("id", "438768422148");
		 requestParams.put("salary", 6000);
		 
		 post_request.body(requestParams.toJSONString());
		 Response post_response = post_request.post("/zain-t3");
		 
		 //After adding the employee, we have to update it's information.
		String empid = "438768422148";
		
		RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
		RequestSpecification request = RestAssured.given();
	
		JSONObject request2Params = new JSONObject();
		request2Params.put("id", "438768422148"); 
		request2Params.put("firstName", "Zaynn"); 
		request2Params.put("lastName", "Hamad");
		request2Params.put("salary", 8000);
		
        // Add a header stating the Request body is a JSON
        request.header("Content-Type", "application/json"); 
 
        // Add the Json to the body of the request
         request.body(request2Params.toJSONString());
 
         Response response = request.put("/zain-t3/"+ empid);
   
         int statusCode = response.getStatusCode();
         System.out.println(response.asString());
         Assert.assertEquals(statusCode, 200);
         
         //After updating the employee information, we have to use GET method and check whether the data in the body is updated or not.
         //Specify base URI
		  RestAssured.baseURI="http://192.168.200.91:8080/demo-server/employee-module";
		  
		  //Request object
		  RequestSpecification httpRequest=RestAssured.given();
		  
		  //Response object
		  Response get_response=httpRequest.request(Method.GET,"/zain-t3");
		  
		  //print response in console window
		  String responseBodyForGet=get_response.getBody().asString();
		  System.out.println("Response Body after updating the employee information by valid ID:" +responseBodyForGet);
		  
		    //after adding and updating, we have use DELETE method to make sure that the server is empty.
		     RestAssured.baseURI = "http://192.168.200.91:8080/demo-server/employee-module";
			 RequestSpecification delete_request = RestAssured.given(); 
			 
			 //Add a header stating the Request body is a JSON
			 delete_request.header("Content-Type", "application/json"); 
			 
			  //Delete the request and check the response
			 Response delete_response = delete_request.delete("/zain-t3"); 
			  int statusCodeOfDelete=delete_response.getStatusCode();
			  System.out.println("Status code is: "+statusCodeOfDelete);
			  Assert.assertEquals(statusCodeOfDelete, 200);
			  
			  BufferedReader br=null;
	          ObjectMapper objectMapper=new ObjectMapper();
			    try {
			    	
			    	  //take the actual data from the server and convert it from JSON format to java object called allInfo
			          AllInfo allInfo= objectMapper.readValue(responseBodyForGet,AllInfo.class);
			          System.out.println("maxSalary= "+ allInfo.getMaxSalary() + "and totalSalaries =" + allInfo.getTotalSalaries() + "and employeesCount =" + allInfo.getEmployeesCount() + "and minSalary =" +allInfo.getMinSalary());
			          
			        
			          if(allInfo != null) {
			          for(Employee e : allInfo.getItems() ) {
			        	  System.out.println(e.getID()+" - "+ e.getFirstName()+" - "+ e.getLastName()+ " - "+ e.getSalary());
			          }
			          }

			          //take the expected data from the file and convert it from JSON format to java object called allInfo_fromFile
			    	  br= new BufferedReader(new FileReader("C:\\Users\\zainh\\eclipse-workspace1\\testEmployeeApi\\ExpectedBodyAfterUpdatingByValidID.txt"));
			    	  AllInfo allInfo_fromFile= objectMapper.readValue(br.toString(),AllInfo.class);
			    	    System.out.println("maxSalary= "+ allInfo_fromFile.getMaxSalary() + "and totalSalaries =" + allInfo_fromFile.getTotalSalaries() + "and employeesCount =" + allInfo_fromFile.getEmployeesCount() + "and minSalary =" +allInfo_fromFile.getMinSalary());
			           
			            if(allInfo_fromFile != null) {
			            for(Employee e1 : allInfo_fromFile.getItems() ) {
			          	  System.out.println(e1.getID()+" - "+ e1.getFirstName()+" - "+ e1.getLastName()+ " - "+ e1.getSalary());
			            }
			            }   
		                
			            //comparing between two objects ( which are allInfo and allInfo_fromFile).
				         IsEqualFunctions r1 = new IsEqualFunctions() ;
				         Assert.assertEquals(true,r1.is_equal_objects(allInfo, allInfo_fromFile));
		          }//end of try
			    
		          catch(FileNotFoundException e) {
		      		e.printStackTrace();
		      	}
}
	
	@Test
	public void updateEmployeeInfoByInValidID_Tput2() throws Exception{
	
		//The server is empty, so we have to POST Employee data and after that update it.
		 RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification post_request = RestAssured.given();
		 
		 JSONObject requestParams = new JSONObject();
		 //Casting
		 requestParams.put("firstName", "Zain"); 
		 requestParams.put("lastName", "Haj Hamad");
		 requestParams.put("id", "438768422148");
		 requestParams.put("salary", 6000);
		 
		 post_request.body(requestParams.toJSONString());
		 Response post_response = post_request.post("/zain-t3");
		 
		 //After adding the employee, we have to update it's information.
		String invalid_empid = "438768422111";
		
		RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
		RequestSpecification request = RestAssured.given();
	
		JSONObject request2Params = new JSONObject();
		request2Params.put("id", "438768422148"); 
		request2Params.put("firstName", "Zaynn"); 
		request2Params.put("lastName", "Hamad");
		request2Params.put("salary", 8000);
		
        // Add a header stating the Request body is a JSON
        request.header("Content-Type", "application/json"); 
 
        // Add the Json to the body of the request
         request.body(request2Params.toJSONString());
 
         Response response = request.put("/zain-t3/"+ invalid_empid);
   
         int statusCode = response.getStatusCode();
         System.out.println(response.asString());
         Assert.assertEquals(statusCode, 200);
         
         //After updating the employee information, we have to use GET method and check whether the data in the body is updated or not.
         //Specify base URI
		  RestAssured.baseURI="http://192.168.200.91:8080/demo-server/employee-module";
		  
		  //Request object
		  RequestSpecification httpRequest=RestAssured.given();
		  
		  //Response object
		  Response get_response=httpRequest.request(Method.GET,"/zain-t3");
		  
		  //print response in console window
		  String responseBodyForGet=get_response.getBody().asString();
		  System.out.println("Response Body after updating the employee information by invalid ID:" +responseBodyForGet);
		  
		  //after adding and updating, we have use DELETE method to make sure that the server is empty.
		     RestAssured.baseURI = "http://192.168.200.91:8080/demo-server/employee-module";
			 RequestSpecification delete_request = RestAssured.given(); 
			 
			 //Add a header stating the Request body is a JSON
			 delete_request.header("Content-Type", "application/json"); 
			 
			  //Delete the request and check the response
			 Response delete_response = delete_request.delete("/zain-t3"); 
			  int statusCodeOfDelete=delete_response.getStatusCode();
			  System.out.println("Status code is: "+statusCodeOfDelete);
			  Assert.assertEquals(statusCodeOfDelete, 200);
			  
			  BufferedReader br=null;
	          ObjectMapper objectMapper=new ObjectMapper();
			    try {
			    	
			    	  //take the actual data from the server and convert it from JSON format to java object called allInfo
			          AllInfo allInfo= objectMapper.readValue(responseBodyForGet,AllInfo.class);
			          System.out.println("maxSalary= "+ allInfo.getMaxSalary() + "and totalSalaries =" + allInfo.getTotalSalaries() + "and employeesCount =" + allInfo.getEmployeesCount() + "and minSalary =" +allInfo.getMinSalary());
			          
			        
			          if(allInfo != null) {
			          for(Employee e : allInfo.getItems() ) {
			        	  System.out.println(e.getID()+" - "+ e.getFirstName()+" - "+ e.getLastName()+ " - "+ e.getSalary());
			          }
			          }

			          //take the expected data from the file and convert it from JSON format to java object called allInfo_fromFile
			    	  br= new BufferedReader(new FileReader("C:\\Users\\zainh\\eclipse-workspace1\\testEmployeeApi\\ExpectedBodyAfterUpdatingByInvalidID.txt"));
			    	  AllInfo allInfo_fromFile= objectMapper.readValue(br.toString(),AllInfo.class);
			    	    System.out.println("maxSalary= "+ allInfo_fromFile.getMaxSalary() + "and totalSalaries =" + allInfo_fromFile.getTotalSalaries() + "and employeesCount =" + allInfo_fromFile.getEmployeesCount() + "and minSalary =" +allInfo_fromFile.getMinSalary());
			           
			            if(allInfo_fromFile != null) {
			            for(Employee e1 : allInfo_fromFile.getItems() ) {
			          	  System.out.println(e1.getID()+" - "+ e1.getFirstName()+" - "+ e1.getLastName()+ " - "+ e1.getSalary());
			            }
			            }   
		                
			            //comparing between two objects ( which are allInfo and allInfo_fromFile).
				         IsEqualFunctions r1 = new IsEqualFunctions() ;
				         Assert.assertEquals(true,r1.is_equal_objects(allInfo, allInfo_fromFile));
		          }//end of try
			    
		          catch(FileNotFoundException e) {
		      		e.printStackTrace();
		      	}
}
		
	}