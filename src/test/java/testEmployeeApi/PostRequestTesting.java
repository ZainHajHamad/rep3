package testEmployeeApi;


import static org.testng.Assert.assertEquals;
import testEmployeeApi.IsEqualFunctions;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PostRequestTesting {


	@Test
	public void addEmployeeUsingUniqueID_Tpost1() throws Exception{
		//Adding an employee
		RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification post_request = RestAssured.given();
		 
		 JSONObject requestParams = new JSONObject();
		 //Casting
		 requestParams.put("firstName", "Zain"); 
		 requestParams.put("lastName", "Haj Hamad");
		 requestParams.put("id", "438768422148");
		 requestParams.put("salary", 6000);
		 
		 post_request.body(requestParams.toJSONString());
		 Response post_response = post_request.post("/zain-t3");
		
		  //After adding an employee, we have to check whether the employee is added or not using GET method
		  RestAssured.baseURI="http://192.168.200.91:8080/demo-server/employee-module";
		  
		  //Request object
		  RequestSpecification httpRequest=RestAssured.given();
		  
		  //Response object
		  Response get_response=httpRequest.request(Method.GET,"/zain-t3");
		  
		  //print response in console window
		  String responseBodyForGet=get_response.getBody().asString();
		  System.out.println("Response Body after adding an employee:" +responseBodyForGet);

		  
		  //after adding, we have use DELETE method to make sure that the server is empty.
		     RestAssured.baseURI = "http://192.168.200.91:8080/demo-server/employee-module";
			 RequestSpecification delete_request = RestAssured.given(); 
			 
			 //Add a header stating the Request body is a JSON
			 delete_request.header("Content-Type", "application/json"); 
			 
			  //Delete the request and check the response
			 Response delete_response = delete_request.delete("/zain-t3"); 
			  int statusCodeOfDelete=delete_response.getStatusCode();
			  System.out.println("Status code is: "+statusCodeOfDelete);
			  Assert.assertEquals(statusCodeOfDelete, 200);
			  
			
	          BufferedReader br=null;
	          ObjectMapper objectMapper=new ObjectMapper();
			    try {
			    	
			    	  //take the actual data from the server and convert it from JSON format to java object called allInfo
			          AllInfo allInfo= objectMapper.readValue(responseBodyForGet,AllInfo.class);
			          System.out.println("maxSalary= "+ allInfo.getMaxSalary() + "and totalSalaries =" + allInfo.getTotalSalaries() + "and employeesCount =" + allInfo.getEmployeesCount() + "and minSalary =" +allInfo.getMinSalary());
			          
			        
			          if(allInfo != null) {
			          for(Employee e : allInfo.getItems() ) {
			        	  System.out.println(e.getID()+" - "+ e.getFirstName()+" - "+ e.getLastName()+ " - "+ e.getSalary());
			          }
			          }

			          //take the expected data from the file and convert it from JSON format to java object called allInfo_fromFile
			    	  br= new BufferedReader(new FileReader("C:\\Users\\zainh\\eclipse-workspace1\\testEmployeeApi\\ExpectedBodyAfterAddingEmployeeByUniqueID.txt"));
			    	  AllInfo allInfo_fromFile= objectMapper.readValue(br.toString(),AllInfo.class);
			    	    System.out.println("maxSalary= "+ allInfo_fromFile.getMaxSalary() + "and totalSalaries =" + allInfo_fromFile.getTotalSalaries() + "and employeesCount =" + allInfo_fromFile.getEmployeesCount() + "and minSalary =" +allInfo_fromFile.getMinSalary());
			           
			            if(allInfo_fromFile != null) {
			            for(Employee e1 : allInfo_fromFile.getItems() ) {
			          	  System.out.println(e1.getID()+" - "+ e1.getFirstName()+" - "+ e1.getLastName()+ " - "+ e1.getSalary());
			            }
			            }   
		              
			        //comparing between two objects ( which are allInfo and allInfo_fromFile).
			         IsEqualFunctions r1 = new IsEqualFunctions() ;
			         Assert.assertEquals(true,r1.is_equal_objects(allInfo, allInfo_fromFile));
			            
		          }//end of try
			    
		          catch(FileNotFoundException e) {
		      		e.printStackTrace();
		      	}
		          
	}

	
	@Test
	public void addEmployeeUsingIDForAnotherEmployee_Tpost2() throws Exception{
		//Adding the first employee
		RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification post_request1 = RestAssured.given();
		 
		 JSONObject requestParams1 = new JSONObject();
		 //Casting
		 requestParams1.put("firstName", "Zain"); 
		 requestParams1.put("lastName", "Haj Hamad");
		 requestParams1.put("id", "438768422148");
		 requestParams1.put("salary", 6000);
		 
		 post_request1.body(requestParams1.toJSONString());
		 Response post_response = post_request1.post("/zain-t3");
		
		 //Adding the second employee that has the same ID of the first employee.
			RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
			 RequestSpecification post_request2 = RestAssured.given();
			 
			 JSONObject requestParams2 = new JSONObject();
			 //Casting
			 requestParams2.put("firstName", "John"); 
			 requestParams2.put("lastName", "Smith");
			 requestParams2.put("id", "438768422148");
			 requestParams2.put("salary", 6000);
			 
			 post_request2.body(requestParams2.toJSONString());
			 Response post_response2 = post_request2.post("/zain-t3");
		 
		 
		  //After adding the first and second employee, we have to check that the second employee is not added using GET method
		  RestAssured.baseURI="http://192.168.200.91:8080/demo-server/employee-module";
		  
		  //Request object
		  RequestSpecification httpRequest=RestAssured.given();
		  
		  //Response object
		  Response get_response=httpRequest.request(Method.GET,"/zain-t3");
		  
		  //print response in console window
		  String responseBodyForGet=get_response.getBody().asString();
		  System.out.println("Response Body after adding the first and second employee with the same ID:" +responseBodyForGet);

		  
		  //after adding, we have use DELETE method to make sure that the server is empty.
		     RestAssured.baseURI = "http://192.168.200.91:8080/demo-server/employee-module";
			 RequestSpecification delete_request = RestAssured.given(); 
			 
			 //Add a header stating the Request body is a JSON
			 delete_request.header("Content-Type", "application/json"); 
			 
			  //Delete the request and check the response
			 Response delete_response = delete_request.delete("/zain-t3"); 
			  int statusCodeOfDelete=delete_response.getStatusCode();
			  System.out.println("Status code is: "+statusCodeOfDelete);
			  Assert.assertEquals(statusCodeOfDelete, 200);
			  
			  BufferedReader br=null;
	          ObjectMapper objectMapper=new ObjectMapper();
			    try {
			    	
			    	  //take the actual data from the server and convert it from JSON format to java object called allInfo
			          AllInfo allInfo= objectMapper.readValue(responseBodyForGet,AllInfo.class);
			          System.out.println("maxSalary= "+ allInfo.getMaxSalary() + "and totalSalaries =" + allInfo.getTotalSalaries() + "and employeesCount =" + allInfo.getEmployeesCount() + "and minSalary =" +allInfo.getMinSalary());
			          
			        
			          if(allInfo != null) {
			          for(Employee e : allInfo.getItems() ) {
			        	  System.out.println(e.getID()+" - "+ e.getFirstName()+" - "+ e.getLastName()+ " - "+ e.getSalary());
			          }
			          }

			          //take the expected data from the file and convert it from JSON format to java object called allInfo_fromFile
			    	  br= new BufferedReader(new FileReader("C:\\Users\\zainh\\eclipse-workspace1\\testEmployeeApi\\ExpectedBodyAfterAddingEmployeeByUsedID.txt"));
			    	  AllInfo allInfo_fromFile= objectMapper.readValue(br.toString(),AllInfo.class);
			    	    System.out.println("maxSalary= "+ allInfo_fromFile.getMaxSalary() + "and totalSalaries =" + allInfo_fromFile.getTotalSalaries() + "and employeesCount =" + allInfo_fromFile.getEmployeesCount() + "and minSalary =" +allInfo_fromFile.getMinSalary());
			           
			            if(allInfo_fromFile != null) {
			            for(Employee e1 : allInfo_fromFile.getItems() ) {
			          	  System.out.println(e1.getID()+" - "+ e1.getFirstName()+" - "+ e1.getLastName()+ " - "+ e1.getSalary());
			            }
			            }
			            
			            //comparing between two objects ( which are allInfo and allInfo_fromFile).
				         IsEqualFunctions r1 = new IsEqualFunctions() ;
				         Assert.assertEquals(true,r1.is_equal_objects(allInfo, allInfo_fromFile));
		          }//end of try
			    
		          catch(FileNotFoundException e) {
		      		e.printStackTrace();
		      	}
	}
	
	@Test
	public void addEmployeeWithoutID_Tpost3() throws Exception{
		//Adding an employee
		RestAssured.baseURI ="http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification post_request = RestAssured.given();
		 
		 JSONObject requestParams = new JSONObject();
		 //Casting without ID
		 requestParams.put("firstName", "Zain"); 
		 requestParams.put("lastName", "Haj Hamad");
		 requestParams.put("salary", 6000);
		 
		 post_request.body(requestParams.toJSONString());
		 Response post_response = post_request.post("/zain-t3");
		
		  //After adding an employee, we have to check whether the employee is added or not using GET method
		  RestAssured.baseURI="http://192.168.200.91:8080/demo-server/employee-module";
		  
		  //Request object
		  RequestSpecification httpRequest=RestAssured.given();
		  
		  //Response object
		  Response get_response=httpRequest.request(Method.GET,"/zain-t3");
		  
		  //print response in console window
		  String responseBodyForGet=get_response.getBody().asString();
		  System.out.println("Response Body after adding an employee:" +responseBodyForGet);

		  BufferedReader br=null;
          ObjectMapper objectMapper=new ObjectMapper();
		    try {
		    	
		    	  //take the actual data from the server and convert it from JSON format to java object called allInfo
		          AllInfo allInfo= objectMapper.readValue(responseBodyForGet,AllInfo.class);
		          System.out.println("maxSalary= "+ allInfo.getMaxSalary() + "and totalSalaries =" + allInfo.getTotalSalaries() + "and employeesCount =" + allInfo.getEmployeesCount() + "and minSalary =" +allInfo.getMinSalary());
		          
		        
		          if(allInfo != null) {
		          for(Employee e : allInfo.getItems() ) {
		        	  System.out.println(e.getID()+" - "+ e.getFirstName()+" - "+ e.getLastName()+ " - "+ e.getSalary());
		          }
		          }

		          //take the expected data from the file and convert it from JSON format to java object called allInfo_fromFile
		    	  br= new BufferedReader(new FileReader("C:\\Users\\zainh\\eclipse-workspace1\\testEmployeeApi\\ExpectedBodyAfterAddingEmployeeWithoutID.txt"));
		    	  AllInfo allInfo_fromFile= objectMapper.readValue(br.toString(),AllInfo.class);
		    	    System.out.println("maxSalary= "+ allInfo_fromFile.getMaxSalary() + "and totalSalaries =" + allInfo_fromFile.getTotalSalaries() + "and employeesCount =" + allInfo_fromFile.getEmployeesCount() + "and minSalary =" +allInfo_fromFile.getMinSalary());
		           
		            if(allInfo_fromFile != null) {
		            for(Employee e1 : allInfo_fromFile.getItems() ) {
		          	  System.out.println(e1.getID()+" - "+ e1.getFirstName()+" - "+ e1.getLastName()+ " - "+ e1.getSalary());
		            }
		            }
		            
		            //comparing between two objects ( which are allInfo and allInfo_fromFile).
			         IsEqualFunctions r1 = new IsEqualFunctions() ;
			         Assert.assertEquals(true,r1.is_equal_objects(allInfo, allInfo_fromFile));
	          }//end of try
		    
	          catch(FileNotFoundException e) {
	      		e.printStackTrace();
	      	}
		  
	}

}
