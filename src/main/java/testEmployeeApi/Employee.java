package testEmployeeApi;


public class Employee {
	
	    public String ID;
	    public String firstName;
	    public String lastName;
	    public int salary;

	    public Employee() {
	    }

	    public Employee(String ID, String firstName, String lastName, int salary) {
	        this.ID = ID;
	        this.firstName = firstName;
	        this.lastName = lastName;
	        this.salary = salary;
	    }

		public String getID() {
			return ID;
		}

		public void setID(String iD) {
			ID = iD;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public int getSalary() {
			return salary;
		}

		public void setSalary(int salary) {
			this.salary = salary;
		}

	}
