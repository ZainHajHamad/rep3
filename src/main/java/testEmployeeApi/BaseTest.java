package testEmployeeApi;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
 
public class BaseTest {
	 private static Logger LOGGER = null;
	
	 @BeforeSuite
		public void getVersionBefore() {
			 BasicConfigurator.configure();
			 LOGGER.info("****Before Suite****");
		}
		
		@BeforeClass
		public void beforeClass() {
			LOGGER.info("****Before Class****");	
		}
		
		@BeforeMethod
		public void beforeTest() {
			LOGGER.info("****Before Test****");	
		}
		
		@AfterMethod
		public void afterTest() {
			LOGGER.info("****After Test****");	
		}
		
		@AfterClass
		public void afterClass() {
			LOGGER.info("****After Class****");	
		}

}
