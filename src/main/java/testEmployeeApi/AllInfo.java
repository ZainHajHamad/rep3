package testEmployeeApi;

import java.util.ArrayList;


public class AllInfo {
 private float maxSalary;
 private float totalSalaries;
 private float employeesCount;
 private float minSalary;
ArrayList < Employee > items = new ArrayList < Employee > ();

 
 public ArrayList<Employee> getItems() {
	return items;
}

public void setItems(ArrayList<Employee> items) {
	this.items = items;
}

public AllInfo(float maxSalary, float totalSalaries, float employeesCount, float minSalary,
		ArrayList<Employee> items) {
	super();
	this.maxSalary = maxSalary;
	this.totalSalaries = totalSalaries;
	this.employeesCount = employeesCount;
	this.minSalary = minSalary;
	this.items = items;
}

public AllInfo() {
    }

 // Getter Methods 

 public float getMaxSalary() {
  return maxSalary;
 }

 public float getTotalSalaries() {
  return totalSalaries;
 }

 public float getEmployeesCount() {
  return employeesCount;
 }

 public float getMinSalary() {
  return minSalary;
 }

 // Setter Methods 

 public void setMaxSalary(float maxSalary) {
  this.maxSalary = maxSalary;
 }

 public void setTotalSalaries(float totalSalaries) {
  this.totalSalaries = totalSalaries;
 }

 public void setEmployeesCount(float employeesCount) {
  this.employeesCount = employeesCount;
 }

 public void setMinSalary(float minSalary) {
  this.minSalary = minSalary;
 }
}

