package testEmployeeApi;

import java.util.ArrayList;

public class IsEqualFunctions {

	 public boolean is_equal_objects(AllInfo obj1, AllInfo obj2) {
		 boolean x=false;
		 boolean y=false;
		 boolean m = false ;
		 boolean result=false;
		 
     if (obj1.getEmployeesCount()==obj2.getEmployeesCount() &&	obj1.getMaxSalary()==obj2.getMaxSalary() && obj1.getMinSalary()==obj2.getMinSalary() && obj1.getTotalSalaries()==obj2.getTotalSalaries()) {
    	 x=true;
    	 System.out.println("min, max salary, count of employees and total salaries are equal.");
     }
     if(is_equal_arrayLists(obj1.items,obj2.items)) {
    	 y=true;
    	 System.out.println("Two array lists are equal to each other.");
     }
     if (x&&y) {
    	 System.out.println("Two objects are equal.");
    	 m = true;
     }
     else {
    	 System.out.println("Two objects are not equal.");
    	 
     }
     return m ; 
     }
     
	 public boolean is_equal_arrayLists(ArrayList<Employee> items1,ArrayList<Employee> items2) {
	 boolean c = false ;
		for (int i =0 ; i < items1.size() ; i++) {
			if ( items1.get(i).getFirstName().equals(items2.get(i).getFirstName()) && items1.get(i).getLastName().equals(items2.get(i).getLastName())
			&& items1.get(i).getID().equals(items2.get(i).getID()) && items1.get(i).getSalary()==items2.get(i).getSalary() ) {
				 c=true; 
	
				}
			else
			 c= false ; 
		}
	
		return c ;
	 }
}
